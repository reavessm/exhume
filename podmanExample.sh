#!/usr/bin/env bash

podman build -f Containerfile -t exhume . && \
podman run \
  --rm \
  --name exhume \
  -it \
  -v ./example.db:/app/example.db:z \
  -v ./example.puml:/app/example.puml:z \
  -v ./migration.sql:/app/migration.sql:z \
  --entrypoint /bin/bash \
  localhost/exhume
