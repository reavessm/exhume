defmodule Exhume.MixProject do
  use Mix.Project

  def project do
    [
      app: :exhume,
      version: "0.1.0",
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: ["lib"],
      # escript: [main_module: Exhume],
      deps: deps(),

      # Docs
      name: "Exhume",
      source_url: "https://gitlab.com/reavessm/exhume",
      dialyzer: [plt_add_apps: [:mix]],
      docs: [
        main: "Exhume",
        extras: ["README.md"]
      ]
      # add releases configuration
      # releases: [
      #   # we can name releases anything, this will be prod's config
      #   exhume: [
      #     # we'll be deploying to Linux only
      #     include_executables_for: [:unix]
      #     # have Mix automatically create a tarball after assembly
      #     # steps: [:assemble, :tar],
      #     # applications: [runtime_tools: :permanent]
      #   ]
      # ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {Exhume.Application, []},
      # env: [d: "example.puml"],
      extra_applications: [:logger, :eex, :runtime_tools]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # Also does struct diff
      {:map_diff, "~> 1.3"},
      {:todo, "~> 1.5"},
      {:nimble_parsec, "~> 1.0"},
      {:ex_doc, "~> 0.27", only: :dev, runtime: false},
      {:dialyxir, "~> 1.4", only: [:dev], runtime: false},
      {:exqlite, "~> 0.23"},
      {:postgrex, "~> 0.19.1"},
      {:jason, "~> 1.0"}
      # {:ecto, "~> 3.10"},
      # {:ecto_sql, "~> 3.0"},
      # {:ecto_sqlite3, "~> 0.16"}
      # {:postgrex, ">= 0.0.0"}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
