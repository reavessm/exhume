# Exhume

![pipeline_badge](https://gitlab.com/reavessm/exhume/badges/main/pipeline.svg)

> NOTE: Exhume is currently a WIP.  To see the current features in action, run
> `mix exhume.diff && cat migrations.sql`

Exhume is a tool to generate SQL DDL from UML.  Eventually, it will
even diff against a live database and generate sql migrations.  

If instead you want to just install it and try it out, you can install the
escript via `mix escript.install git https://gitlab.com/reavessm/exhume`

## Usage

Choose any two parsers and exhume will diff them and generate SQL code based on the diff, for example

```
┌──────────┐   ┌────────┐   ┌──────┐                          
│ PlantUML ├───┤ Parser ├───┤      │                          
└──────────┘   └────────┘   │      │   ┌───────────┐   ┌─────┐
                            │ Diff ├───┤ Generator ├───┤ SQL │
┌──────────┐   ┌────────┐   │      │   └───────────┘   └─────┘
│ Sqlite   ├───┤ Parser ├───┤      │                          
└──────────┘   └────────┘   └──────┘
```

Future versions may allow for things like

```
┌──────────┐   ┌────────┐   ┌──────┐                          
│ Mermaid  ├───┤ Parser ├───┤      │                          
└──────────┘   └────────┘   │      │   ┌───────────┐   ┌─────┐
                            │ Diff ├───┤ Generator ├───┤ SQL │
┌──────────┐   ┌────────┐   │      │   └───────────┘   └─────┘
│ Postgres ├───┤ Parser ├───┤      │                          
└──────────┘   └────────┘   └──────┘
```

## Why Elixir

This tool is written in Elixir and that provides a couple of benefits:
  - NimbleParsec
  - EEx
  - Structs as Maps
