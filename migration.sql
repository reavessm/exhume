ALTER TABLE
    Entity01
ADD
    another_optional_attribute varchar(5);

CREATE TABLE IF NOT EXISTS Entity02 (
    mandatory_attribute int NOT NULL,
    optional_attribute int
);

CREATE TABLE IF NOT EXISTS Entity03 (
    bar text NOT NULL,
    foo uuid NOT NULL,
    baz int,
    PRIMARY KEY (bar, foo)
);
