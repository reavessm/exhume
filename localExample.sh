#!/usr/bin/env bash

rm -fv migration.sql

export MIX_ENV=prod
mix release --force --overwrite

export DESIRED=example.puml
export ACTUAL=example.db
# export DESIRED=web-rca.puml
# export ACTUAL=postgresql://web-rca-user:dog8code@localhost/web-rca
export OUTPUT=migration.sql
_build/prod/rel/exhume/bin/exhume start

cat migration.sql
