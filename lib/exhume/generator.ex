defmodule Exhume.Generator do
  alias Exhume.Model.Class
  alias Exhume.Model.Table

  # @callback class_to_string(%Class{}) :: String.t()
  @callback table_to_string(%Table{}) :: String.t()
  @callback generate(%{classes: [%Class{}], tables: [%Table{}]}) :: String.t()
end
