defmodule Exhume.Parser do
  # alias Exhume.Model.Class
  # alias Exhume.Model.Class.Attribute
  # alias Exhume.Model.Class.Operation

  # @callback parse(binary(), keyword()) ::
  #             {:ok, [term()], rest, context, line, byte_offset}
  #             | {:error, reason, rest, context, line, byte_offset}
  #           when line: {pos_integer(), byte_offset},
  #                byte_offset: pos_integer(),
  #                rest: binary(),
  #                reason: String.t(),
  #                context: map()
  @callback to_model(String.t()) :: %{
              tables: []
            }

  def get_parser(name) do
    case name do
      "plant-uml" ->
        &Exhume.Parser.PlantUML.read/1

      "sqlite" ->
        &Exhume.Parser.Sqlite.to_model/1

      p when p in ["pg", "postgres", "postgresql", "postgrex"] ->
        &Exhume.Parser.Postgresql.to_model/1

      _ ->
        prefix =
          cond do
            String.contains?(name, ":") ->
              String.split(name, ":") |> List.first()

            true ->
              ""
          end

        suffix =
          cond do
            String.contains?(name, ".") ->
              Enum.reverse(String.split(name, ".")) |> List.first()

            true ->
              ""
          end

        case suffix do
          s when s in ["puml", "plant-uml"] ->
            &Exhume.Parser.PlantUML.read/1

          "db" ->
            &Exhume.Parser.Sqlite.to_model/1

          _ ->
            case prefix do
              "postgresql" ->
                &Exhume.Parser.Postgresql.to_model/1
            end
        end
    end
  end
end
