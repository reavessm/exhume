defmodule Exhume.Parser.PlantUML do
  @behaviour Exhume.Parser

  alias Exhume.Model.Table

  import NimbleParsec

  _word = utf8_string([?a..?z, ?A..?Z, ?*], min: 1)
  entityName = utf8_string([?a..?z, ?A..?Z, ?0..?9, ?*], min: 1)
  field_line = utf8_string([?a..?z, ?A..?Z, ?0..?9, ?\s, ?:, ?(, ?), ?_], min: 1)

  start =
    string("@")
    |> concat(string("startuml"))
    |> repeat(string("\n"))

  entity_start =
    ignore(optional(string("\n")))
    |> ignore(string("entity "))
    |> concat(entityName)
    |> ignore(string(" {"))
    |> tag(:name)

  mandatory_column =
    ignore(optional(repeat(string("\n"))))
    |> ignore(optional(repeat(string(" "))))
    |> ignore(string("* "))
    |> concat(field_line)
    |> ignore(string("\n"))

  column =
    ignore(optional(repeat(string("\n"))))
    |> ignore(optional(repeat(string(" "))))
    |> concat(field_line)
    |> ignore(string("\n"))

  entity_end =
    ignore(optional(string("\n")))
    |> ignore(repeat(string(" ")))
    |> ignore(string("}"))
    |> ignore(repeat(string("\n")))

  defparsec(
    :parse,
    ignore(start)
    |> repeat(
      entity_start
      |> optional(
        repeat(
          mandatory_column
          |> ignore(repeat(string(" ")))
        )
        |> ignore(string("--"))
        |> tag(:key)
      )
      |> repeat(
        choice([
          column
          |> tag(:column),
          mandatory_column
          |> tag(:mandatory_column)
        ])
      )
      |> concat(entity_end)
      |> tag(:table)
    )
  )

  def read(filename) do
    to_model(File.read!(filename))
  end

  def to_model(content) do
    {:ok, parsed, _rest, _context, _line, _byte_offset} =
      parse(content)

    tables =
      parsed
      |> Enum.reduce([], fn i, acc ->
        case i do
          {:table, table} ->
            t =
              table
              |> Enum.reduce(%{name: "", keys: [], mandatory_columns: [], columns: []}, fn x,
                                                                                           ac ->
                case x do
                  {:name, [name | []]} ->
                    %{
                      name: name,
                      keys: ac[:keys],
                      mandatory_columns: ac[:mandatory_columns],
                      columns: ac[:columns]
                    }

                  {:key, key} ->
                    %{
                      name: ac[:name],
                      keys: ac[:keys] ++ [key],
                      mandatory_columns: ac[:mandatory_columns],
                      columns: ac[:columns]
                    }

                  {:mandatory_column, c} ->
                    %{
                      name: ac[:name],
                      keys: ac[:keys],
                      mandatory_columns: ac[:mandatory_columns] ++ [c],
                      columns: ac[:columns]
                    }

                  {:column, c} ->
                    %{
                      name: ac[:name],
                      keys: ac[:keys],
                      mandatory_columns: ac[:mandatory_columns],
                      columns: ac[:columns] ++ [c]
                    }
                end
              end)

            acc ++ [t]

          _ ->
            nil
        end
      end)

    Table.from_list_of_maps(tables)
    |> Enum.reduce(%{}, fn x, acc ->
      Map.put(acc, x[:name], x)
    end)
  end
end
