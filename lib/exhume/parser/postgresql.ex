defmodule Exhume.Parser.Postgresql do
  alias Exhume.Model.Table

  import NimbleParsec

  word = utf8_string([?a..?z, ?A..?Z, ?-, ?0..?9], min: 1)
  start = string("postgresql://")

  user =
    word
    |> ignore(utf8_string([?:, ?@], min: 1))
    |> tag(:user)

  password =
    word
    |> ignore(string("@"))
    |> tag(:pass)

  location = word |> tag(:location)

  port =
    ignore(string(":"))
    |> concat(word)
    |> tag(:port)

  db =
    ignore(string("/"))
    |> concat(word)
    |> tag(:db)

  defparsec(
    :parse,
    ignore(start)
    |> optional(user)
    |> optional(password)
    |> optional(location)
    |> optional(port)
    |> optional(db)
  )

  def to_model(connstring) do
    {:ok, [user: [user], pass: [pass], location: [location], db: [db]], _, _, _, _} =
      parse(connstring)

    conn =
      Postgrex.start_link(
        hostname: location,
        username: user,
        database: db,
        password: pass
      )

    {:ok, pid} = conn

    schemas =
      Postgrex.query!(
        pid,
        "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';",
        []
      )

    Map.get(schemas, :rows)
    |> Enum.reduce([], fn [t], table_list ->
      %{rows: rows} =
        Postgrex.query!(
          pid,
          "SELECT column_name, data_type, is_nullable FROM information_schema.columns WHERE table_name = $1;",
          [t]
        )

      table_list ++
        [
          rows
          |> Enum.reduce(
            %{
              name: t,
              keys: [],
              mandatory_columns: [],
              columns: []
            },
            fn [
                 column_name,
                 data_type,
                 is_nullable
               ],
               acc ->
              case is_nullable do
                "YES" ->
                  %{
                    name: t,
                    keys: [],
                    mandatory_columns: acc[:mandatory_columns],
                    columns: acc[:columns] ++ [column_name <> " : " <> data_type]
                  }

                _ ->
                  %{
                    name: t,
                    keys: [],
                    mandatory_columns:
                      acc[:mandatory_columns] ++ [column_name <> " : " <> data_type],
                    columns: acc[:columns]
                  }
              end
            end
          )
        ]
    end)
    |> Table.from_list_of_maps()
    |> Enum.reduce(%{}, fn x, acc ->
      Map.put(acc, x[:name], x)
    end)
  end
end
