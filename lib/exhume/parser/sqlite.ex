defmodule Exhume.Parser.Sqlite do
  def to_model(filename) do
    {:ok, conn} = Exqlite.Sqlite3.open(filename)

    {:ok, statement} =
      Exqlite.Sqlite3.prepare(conn, "SELECT tbl_name FROM sqlite_schema WHERE type = 'table';")

    tables =
      get_all(conn, statement, [])
      |> List.flatten()
      |> Enum.reduce([], fn t, ts ->
        query = "SELECT * FROM pragma_table_info('" <> t <> "');"

        {:ok, statement} =
          Exqlite.Sqlite3.prepare(conn, query)

        table =
          get_all(conn, statement, [])
          |> Stream.map(fn x -> Enum.zip([:col_id, :name, :type, :not_null, :default, :pk], x) end)
          |> Enum.reduce(%{name: t, keys: [], mandatory_columns: [], columns: []}, fn x, acc ->
            name = x[:name] <> " : " <> x[:type]

            case x[:pk] do
              0 ->
                case x[:not_null] do
                  1 ->
                    %{
                      name: acc[:name],
                      keys: acc[:keys],
                      mandatory_columns: acc[:mandatory_columns] ++ [name |> String.downcase()],
                      columns: acc[:columns]
                    }

                  _ ->
                    %{
                      name: acc[:name],
                      keys: acc[:keys],
                      mandatory_columns: acc[:mandatory_columns],
                      columns: acc[:columns] ++ [name |> String.downcase()]
                    }
                end

              _ ->
                %{
                  name: acc[:name],
                  keys: acc[:keys] ++ [name |> String.downcase()],
                  mandatory_columns: acc[:mandatory_columns],
                  columns: acc[:columns]
                }
            end
          end)

        ts ++ [table]
      end)

    tables
    |> Exhume.Model.Table.from_list_of_maps()
    |> Enum.reduce(%{}, fn x, acc ->
      Map.put(acc, x[:name], x)
    end)

    # tables =
    #   tables
    #   |> Exhume.Model.Table.from_list_of_maps()
    #   |> Enum.reduce(%{}, fn x, acc ->
    #     Map.put(acc, x[:name], x)
    #   end)
  end

  def get_all(conn, statement, rows) do
    case Exqlite.Sqlite3.step(conn, statement) do
      {:row, data} ->
        get_all(conn, statement, [data] ++ rows)

      :done ->
        :ok = Exqlite.Sqlite3.release(conn, statement)
        rows
    end
  end
end
