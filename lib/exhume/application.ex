defmodule Exhume.Application do
  use Application

  @impl Application
  def start(_type, _args) do
    {:ok, Process.spawn(fn -> Exhume.main() end, [])}
  end

  @impl Application
  def stop(_state) do
    System.stop()
  end
end
