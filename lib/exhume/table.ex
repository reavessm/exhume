defmodule Table do
  @moduledoc since: "0.1.0"

  @doc """
  The Table struct represents a table in a database
  """
  defstruct [:name, :type]
end
