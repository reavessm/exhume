defmodule Exhume.Generator.Sql do
  alias Exhume.Model.Migration

  @mandatory ~s"
    <%= for mc <- mandatory_columns do %>
      <%= mc %> NOT NULL,
    <% end %>"

  @columns ~s"
    <%= for c <- columns do %>
      <%= c %>,
    <% end %>"

  # TODO: This is wrong
  @keys ~s",
    PRIMARY KEY (
      <%=  keys  %>
    )"

  @add_table ~s"CREATE TABLE IF NOT EXISTS <%= table_name %> (
    <%= columns %>
    <%= pk %>
);"

  @add_column ~s"ALTER TABLE <%= table_name %> ADD <%= column %>;"

  def generate([]), do: nil

  def generate(migs = [%Migration{} | _]) do
    Enum.reduce(migs, "", fn x, acc ->
      cond do
        x.add_columns != nil ->
          "#{acc} #{EEx.eval_string(@add_column, table_name: x.table.name, column: x.add_columns)}"
          |> String.replace(":", "")

        x.table != nil ->
          columns =
            "#{EEx.eval_string(@mandatory, mandatory_columns: x.table.keys)}
            #{EEx.eval_string(@mandatory,
            mandatory_columns: x.table.mandatory_columns,
            trim: true)}
            #{EEx.eval_string(@columns, columns: x.table.columns, trim: true)}"
            |> String.trim()
            |> String.trim("\n")
            |> String.trim(",")
            |> String.replace(":", "")

          pk =
            if x.table.keys == [] do
              ""
            else
              "#{EEx.eval_string(@keys,
              keys: x.table.keys |> Enum.reduce("", fn y, ac -> ac <> (y |> String.split(":") |> List.first() |> String.trim()) <> "," end) |> String.trim(","),
              trim: true)}"
            end

          "#{acc} #{EEx.eval_string(@add_table, table_name: x.table.name, columns: columns, pk: pk, trim: true)}"

        true ->
          acc
      end
    end)
  end
end
