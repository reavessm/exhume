defmodule Exhume.Generator.Mermaid do
  alias Exhume.Model.Class
  alias Exhume.Model.Table

  @behaviour Exhume.Generator

  @class ~s"classDiagram<%= for c <- classes do %>
  <%= c %>
  <% end %>
"

  def class_to_string(c = %Class{}) do
    class = "class #{c.name} {\n"

    class =
      c.attributes
      |> Enum.reduce(class, fn x, acc -> acc <> "\t#{x.type} #{x.value}\n" end)

    class =
      c.operations
      |> Enum.reduce(class, fn x, acc ->
        acc <> "\t#{x.return} #{x.name}(#{x.inputs |> Enum.join(", ")})\n"
      end)

    class <> "  }"
  end

  def table_to_string(%Table{}) do
    # ERDs are jank
    ""
  end

  def generate(%{classes: classes, tables: tables}) do
    EEx.eval_string(@class,
      classes:
        classes
        |> Enum.map(fn x -> class_to_string(x) end),
      tables: tables |> Enum.map(fn x -> table_to_string(x) end),
      trim: true
    )
  end
end
