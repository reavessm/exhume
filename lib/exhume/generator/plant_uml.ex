defmodule Exhume.Generator.PlantUML do
  # alias Exhume.Model.Class
  alias Exhume.Model.Table

  @behaviour Exhume.Generator

  @uml ~s"@startuml
<%= for t <- tables do %>
<%= t %>
<% end %>
@enduml
"

  def table_to_string(t = %Table{}) do
    table = "entity #{t.name} {\n"

    table =
      t.keys
      |> Enum.reduce(table, fn x, acc -> acc <> "  * #{x}\n" end)

    table =
      table <>
        case List.first(t.keys) do
          nil -> ""
          _ -> " --\n"
        end

    table =
      t.mandatory_columns
      |> Enum.reduce(table, fn x, acc -> acc <> "  * #{x}\n" end)

    table =
      t.columns
      |> Enum.reduce(table, fn x, acc -> acc <> "  #{x}\n" end)

    table <> "}"
  end

  def generate(tables) do
    EEx.eval_string(@uml,
      tables: tables |> Enum.map(fn x -> table_to_string(x) end),
      trim: true
    )
  end
end
