defmodule Exhume.Model.Migration do
  defstruct [:table, :add_columns, :drop_columns]

  def from_diffs(ds = [%Exhume.Model.Diff{} | _]) do
    Enum.reduce(ds, [%Exhume.Model.Migration{}], fn x, acc ->
      case {x.removed, x.added} do
        # No change, keep going
        {nil, nil} ->
          acc

        {nil, added} when added != nil ->
          acc ++
            [
              %Exhume.Model.Migration{
                table: added
              }
            ]

        {removed, nil} when removed != nil ->
          # I don't think this can happen?
          acc ++ []

        {removed, added} ->
          value = MapDiff.diff(removed, added).value

          if value.name.changed != :equal do
            nil
          end

          if value.keys.changed != :equal do
            nil
          end

          acc =
            if value.columns.changed != :equal do
              acc ++
                [
                  %Exhume.Model.Migration{
                    table: added,
                    add_columns: value.columns.added -- value.columns.removed
                  }
                ]
            else
              acc
            end

          if value.mandatory_columns.changed != :equal do
            nil
          end

          acc
      end
    end)
  end
end
