defmodule Exhume.Model.Table do
  defstruct [:name, :keys, :mandatory_columns, :columns]

  alias Exhume.Model.Table

  @behaviour Access

  def from_list_of_maps([]) do
    %Table{name: "", keys: [], mandatory_columns: [], columns: []}
  end

  @spec from_list_of_maps([map()]) :: [
          %Table{
            :name => String.t(),
            :keys => [
              [String.t()]
            ],
            :mandatory_columns => [
              [String.t()]
            ],
            :columns => [
              [String.t()]
            ]
          }
        ]
  def from_list_of_maps(list_of_maps = [%{name: _, keys: _, columns: _} | _]) do
    list_of_maps
    |> Enum.map(fn map ->
      %Table{
        name: map[:name],
        keys: map[:keys] |> List.flatten() |> Enum.sort(),
        mandatory_columns: map[:mandatory_columns] |> List.flatten() |> Enum.sort(),
        columns: map[:columns] |> List.flatten() |> Enum.sort()
      }
    end)
  end

  @impl Access
  def fetch(term, key) do
    Map.fetch(term, key)
  end

  @impl Access
  def pop(data, key) do
    Map.pop(data, key)
  end

  @impl Access
  def get_and_update(data, key, function) do
    Map.get_and_update(data, key, function)
  end
end
