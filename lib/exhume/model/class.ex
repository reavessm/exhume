defmodule Exhume.Model.Class do
  defstruct [:name, :attributes, :operations]

  alias Exhume.Model.Class
  alias Exhume.Model.Class.Attribute
  alias Exhume.Model.Class.Operation

  @doc false
  def from_list_of_maps([]) do
    %Class{name: "", attributes: [], operations: []}
  end

  @doc """
  Creates a Class from a list of maps

  ## Examples

      iex> [
      ...>   %{
      ...>     attributes: [["Int", "i"], ["char", "data"], ["char*", "foo"]],
      ...>     name: "Bar",
      ...>     operations: [
      ...>       ["int", "operation", "int"],
      ...>       ["int", "operationTwo", "int", "int"],
      ...>       ["void", "another"]
      ...>     ]
      ...>   },
      ...>   %{
      ...>     attributes: [["String", "data"], ["Integer", "i"]],
      ...>     name: "Dummy",
      ...>     operations: [["void", "methods"]]
      ...>   }
      ...> ] |> Class.from_list_of_maps()
      [
        %Class{
          name: "Bar",
          attributes: [
            %Attribute{type: "Int", value: "i"},
            %Attribute{type: "char", value: "data"},
            %Attribute{type: "char*", value: "foo"}
          ],
          operations: [
            %Operation{return: "int", name: "operation", inputs: ["int"]},
            %Operation{
              return: "int",
              name: "operationTwo",
              inputs: ["int", "int"]
            },
            %Operation{return: "void", name: "another", inputs: []}
          ]
        },
        %Class{
          name: "Dummy",
          attributes: [
            %Attribute{type: "String", value: "data"},
            %Attribute{type: "Integer", value: "i"}
          ],
          operations: [%Operation{return: "void", name: "methods", inputs: []}]
        }
      ]
  """
  @spec from_list_of_maps([map()]) :: [
          %Class{
            :name => String.t(),
            :attributes => [
              %Exhume.Model.Class.Attribute{
                :type => String.t(),
                :value => String.t()
              }
            ],
            :operations => [
              %Operation{
                :return => String.t(),
                :name => String.t(),
                :inputs => [String.t()]
              }
            ]
          }
        ]
  def from_list_of_maps(list_of_maps = [%{name: _, attributes: _, operations: _} | _]) do
    list_of_maps
    |> Enum.map(fn map ->
      %Class{
        name: map[:name],
        attributes:
          map[:attributes]
          |> Enum.map(fn i ->
            List.to_tuple(i)
            |> Attribute.from_tuple()
          end),
        operations:
          map[:operations]
          |> Enum.map(fn i -> Operation.from_list(i) end)
      }
    end)
  end
end
