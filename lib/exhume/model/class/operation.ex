defmodule Exhume.Model.Class.Operation do
  alias Exhume.Model.Class.Operation

  defstruct [:return, :name, :inputs]

  @doc false
  def from_list([]) do
    %Operation{}
  end

  @doc false
  def from_list([name | []]) do
    %Operation{
      return: "void",
      name: name,
      inputs: []
    }
  end

  @doc false
  def from_list([return, name | []]) do
    %Operation{
      return: return,
      name: name,
      inputs: []
    }
  end

  @doc """
  Create an Operation from a list.

  If the list only contains one element, its assumed that the sole element is the name of the Operation.

  If the list contains two or more elements, The first element will be the return value, the second the name, and everything else will be the inputs

  ## Examples

      iex> Operation.from_list([])
      %Operation{} 

      iex> Operation.from_list(["foo"])
      %Operation{name: "foo", return: "void", inputs: []} 

      iex> Operation.from_list(["String", "foo", "int", "int"])
      %Operation{
        name: "foo",
        return: "String",
        inputs: ["int", "int"]
      } 
  """
  @spec from_list([String.t(), ...]) ::
          %Operation{
            :return => String.t(),
            :name => String.t(),
            :inputs => nonempty_list :: [String.t()]
          }
  def from_list([return, name | inputs]) do
    %Operation{
      return: return,
      name: name,
      inputs: inputs
    }
  end
end
