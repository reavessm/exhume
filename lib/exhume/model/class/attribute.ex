defmodule Exhume.Model.Class.Attribute do
  alias Exhume.Model.Class.Attribute
  defstruct [:type, :value]

  @doc """
  Construct an Attribute from a tuple

  ## Examples

      iex> Attribute.from_tuple({"String", "foo"})
      %Attribute{
        type: "String",
        value: "foo"
      }
  """
  @spec from_tuple({String.t(), String.t()}) :: %Attribute{
          :type => String.t(),
          :value => String.t()
        }
  def from_tuple({t, v}) do
    %Attribute{type: t, value: v}
  end
end
