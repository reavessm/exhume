defmodule Exhume.Model.Diff do
  defstruct [:removed, :added, :changed]

  alias Exhume.Model.Diff

  def from_list_of_maps([]) do
    []
  end

  def from_list_of_maps(list_of_maps = [%{removed: _, added: _} | _]) do
    list_of_maps
    |> Enum.map(fn map ->
      %Diff{
        removed: map[:removed],
        added: map[:added]
      }
    end)
  end

  # TODO: Tidy this up
  def diff(desired, actual) do
    Enum.reduce(desired, %{}, fn {name, table = %Exhume.Model.Table{}}, acc ->
      Map.put(acc, name, MapDiff.diff(actual[name], table))
    end)
    |> Enum.reduce([], fn {_k, v}, acc ->
      acc ++
        [
          %Diff{
            removed: v[:removed],
            added: v[:added],
            changed: v[:changed]
          }
        ]
    end)
  end
end
