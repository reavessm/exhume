defmodule Mix.Tasks.Exhume.DiffPg do
  use Mix.Task

  # TODO: Turn this into a parser
  def run(_args) do
    System.put_env("DESIRED", System.get_env("DESIRED", "example.puml"))
    System.put_env("DESIRED_TYPE", System.get_env("DESIRED_TYPE", "plant-uml"))

    System.put_env(
      "ACTUAL",
      System.get_env("ACTUAL", "postgresql://web-rca-user:dog8code@localhost/web-rca")
    )

    System.put_env("ACTUAL_TYPE", System.get_env("ACTUAL_TYPE", "pg"))
    System.put_env("OUTPUT", System.get_env("OUTPUT", "migration.sql"))
    System.put_env("OUTPUT_TYPE", System.get_env("OUTPUT_TYPE", "sql"))

    # {desired, desired_type, actual, actual_type, output, output_type} = Exhume.parse_args()
    {desired, _, actual, _, output, _} = Exhume.parse_args()

    IO.inspect(desired)
    IO.inspect(actual)
    IO.inspect(output)

    tables = Exhume.Parser.Postgresql.to_model(actual)

    IO.inspect(tables)

    {:ok, content} = File.read(desired)

    desired =
      content
      |> Exhume.Parser.PlantUML.to_model()

    generated =
      Exhume.Model.Diff.diff(desired, tables)
      |> Exhume.Model.Migration.from_diffs()
      |> Exhume.Generator.Sql.generate()

    File.write!(output, generated)

    try do
      System.cmd("sleek", ["-n", output])
    rescue
      e ->
        case e do
          %ErlangError{original: :enoent} ->
            IO.puts("Sleek command not installed, please 'cargo install sleek' to format sql.")

          _ ->
            IO.inspect(e)
            reraise(e, __STACKTRACE__)
        end
    end
  end
end
