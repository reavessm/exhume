defmodule Exhume do
  def main() do
    {desired, desired_type, actual, actual_type, output, output_type} = parse_args()

    # TODO get parser to handle input
    File.write!(
      output,
      Exhume.Model.Diff.diff(desired_type.(desired), actual_type.(actual))
      |> Exhume.Model.Migration.from_diffs()
      |> output_type.()
    )

    IO.puts(IO.ANSI.green() <> "Migration written to " <> output <> IO.ANSI.reset())

    try do
      System.cmd("sleek", ["-n", output])
    rescue
      e ->
        case e do
          %ErlangError{original: :enoent} ->
            IO.puts("Sleek command not installed, please 'cargo install sleek' to format sql.")

          _ ->
            IO.inspect(e)
            reraise(e, __STACKTRACE__)
        end
    end
  end

  def parse_args() do
    desired = System.get_env("DESIRED")
    actual = System.get_env("ACTUAL")
    output = System.get_env("OUTPUT")

    cond do
      desired == "" || desired == nil -> raise err_msg("desired", "example.puml")
      actual == "" || actual == nil -> raise err_msg("actual", "example.db")
      output == "" || output == nil -> raise err_msg("output", "migration.sql")
      true -> nil
    end

    # TODO: Implement help/usage

    desired_type =
      Exhume.Parser.get_parser(System.get_env("DESIRED_TYPE", System.get_env("DESIRED")))

    actual_type =
      Exhume.Parser.get_parser(System.get_env("ACTUAL_TYPE", System.get_env("ACTUAL")))

    # TODO: Move to generator module
    output_type =
      case System.get_env("OUTPUT_TYPE", "sql") do
        "sql" -> &Exhume.Generator.Sql.generate/1
        _ -> raise err_msg("output_type", "sql")
      end

    {desired, desired_type, actual, actual_type, output, output_type}
  end

  @moduledoc """
  Documentation for `Exhume`.
  """

  # alias DBConnection.App

  use TODO

  @todo "0.2.0": "UML/ERD -> struct",
        "0.2.0": "This requires lexer/parser",
        "0.3.0": "GORM migrations -> UML/ERD/struct",
        "1.0.0": "struct diff -> GORM migration"

  @doc """
  Show an example of MapDiff

  ## Examples

       iex> Exhume.test_diff() 
       %{
          added: %Table{name: "bar", type: :string},
          changed: :map_change,
          removed: %Table{name: "foo", type: :string},
          struct_name: Table,
          value: %{
            name: %{added: "bar", changed: :primitive_change, removed: "foo"},
            type: %{changed: :equal, value: :string}
          }
        }
  """
  def test_diff() do
    a = %Table{name: "foo", type: :string}
    b = %Table{name: "bar", type: :string}

    MapDiff.diff(a, b)
  end

  def test_gen() do
    {:ok, content} = File.read("example.puml")

    :ok =
      File.write(
        "output.puml",
        content
        |> Exhume.Parser.PlantUML.to_model()
        |> Exhume.Generator.PlantUML.generate()
      )

    IO.puts("UML file written to output.puml")
  end

  def err_msg(missing, consider),
    do: "Invalid or missing '" <> missing <> "' argument, consider '" <> consider <> "'."
end
